package ru.geekbrains;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // количество вершин графа
        int countVertices = 7;
        // генерируем случайную матрицу смежности
        int[][] adjacencyMatrix = generateGraphMatrix(countVertices);
        // печатаем матрицу смежности
        printGraphMatrix(adjacencyMatrix);
        // обходим граф в ширину начиная с вершины 0
        task3(0, adjacencyMatrix);

        // Рекурсивный обход графа в глубину.
        boolean[] visited = new boolean[adjacencyMatrix.length];
        task2(0, adjacencyMatrix, visited);
        // Стоит учесть, что если в массиве visited после переачи его в метод task2 останутся значение false,
        // то это свидетельствует о том, что граф является не связанным и состоит из несколькох компонент связанности
    }

    /**
     * Задача 2
     * Написать рекурсивную функцию обхода графа в глубину.
     */
    public static void task2(int v, int[][] adjacencyMatrix, boolean[] visited) {
        if (visited[v]) return;
        visited[v] = true;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            int u = adjacencyMatrix[v][i];
            if (u == 1 && !visited[i]) {
                task2(i, adjacencyMatrix, visited);
            }
        }
    }

    /**
     * Задача 3
     * Написать функцию обхода графа в ширину.
     */
    public static void task3(int v, int[][] adjacencyMatrix) {
        Queue<Integer> queue = new LinkedList<Integer>();
        boolean[] visited = new boolean[adjacencyMatrix.length];
        visited[v] = true;
        queue.add(v);
        while (!queue.isEmpty()) {
            final int k = queue.poll();
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                final int u = adjacencyMatrix[k][i];
                if (u == 1 && !visited[i]) {
                    visited[i] = true;
                    queue.add(i);
                }
            }
        }
        // проверяем все ли точки графа были посещены, если не все, то это свидетельствует о том,
        // что граф является не связанным и состоит из несколькох компонент связанности
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (!visited[i]) task3(i, adjacencyMatrix);
        }
    }

    /**
     * Генерирует случайную мартрицу смежности графа.
     */
    public static int[][] generateGraphMatrix(int countVertices) {
        if (countVertices < 3) throw new IllegalArgumentException("Минимальное занчение для countVertices - 3!");
        int[][] adjacencyMatrix = new int[countVertices][countVertices];
        for (int i = 1; i < countVertices; i++) {
            final int maxEdges = (i + 1) / 2;
            int countEdges = (int) (Math.random() * maxEdges) + 1;
            while (countEdges != 0) {
                final int v2 = (int) (Math.random() * i);
                if (v2 == i || adjacencyMatrix[i][v2] > 0) continue;
                adjacencyMatrix[i][v2] = 1;
                adjacencyMatrix[v2][i] = 1;
                countEdges--;
            }
        }
        return adjacencyMatrix;
    }

    /**
     * Печатает матрицу смежности графа.
     */
    public static void printGraphMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
    }
}
